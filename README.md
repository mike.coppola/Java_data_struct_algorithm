This is a small Java project I used to exercise my Data structure and algorithm skills. 
Below are the 3 problems I solved and the requirements for each.

-------------
Demonstrated:
--------------
- Binary Search Tree
- LinkedHashSet
- Recursion
- Graph Traversal (DFS: (Depth First Search) )
- Java Unit Tests



=============================================================
Problem #1: CRD  (Create, Read and Delete operations)
============================================================



1. Create: Implement method named saveUrl(userToken, URL)

-------
Input:
-------
userToken(String), URL(String)

---------
Return: 
---------
A boolean value of whether or not the URL was successfully saved. 
If the URL has been saved for the user previously, this function
should not save it and return false. 
2. Read: Implement method called getUrls(userToken)


-------
Input: 
--------
userToken(String)

--------
Return: 
--------
A collection of all the URLs that user has saved, if any.
3. Delete: Implement method called removeUrl(userToken, URL)


-------
Input: 
-------
userToken(String), URL(String)

-------
Return: 
-------
A boolean value of whether or not the URL was successfully deleted. 
If the URL to be deleted had never been saved, the function should 
return false.



===========================
Problem #2. Domain Specific
===========================

Build a function called getUsersByDomain(domain) which takes in a domain name String (e.g. "espn.com") and returns a collection of user tokens.

-------------
Assumptions:
-------------
Eensure that your implementation is optimized for run-time performance given the increased scale of your user base.
Imagine we’ve already implemented a function extractDomain() that when given a String input of a URL, returns the domain name. 
For example, extractDomain("http://www.espn.com/story1") will return "espn.com". You don’t need to worry about subdomains.
Only use current data. If a user had saved a URL, but removed it, it should not be returned here.
Implement getUsersByDomain(domain)

-------
Input: 
-------
domain(String)

--------
Return: 
--------
A collection of user tokens who have saved URLs that belong to that domain.


=============================
Problem #3 : Traverse a Graph
=============================
Build a new feature that will display a list of
recommended URLs based on the URL that is currently being viewed by the user. A a relevance graph has been created where each node represents a unique URL.
Every node has up to three edges (or "hops") to other nodes.
Let’s name these edges A , B , and C. Nodes that are less hops away from each other are deemed to be more related.
Conversely, when they are separated by a greater number of “hops”, they are deemed less related.

The optimal set of recommended URLs to display are the URLs who represent all nodes in the graph that are exactly three edge traversals
or “hops” away from the node that represents the URL that is currently being viewed by the user
(i.e. content that’s similar, but not too similar). Your job is to write a function that given an input URL, can traverse the graph
and return the full set of nodes that are exactly three "hops" away from the input URL's node in the graph.



-------------
Assumptions:
-------------
The function getNode(url) returns the Node object that represents the URL in our relevance graph. Assume this function has already been written and is available to you.
Every Node object has the following functions:

-----------------------------------------------------------------------------------------------------------
A(): returns the Node object that exists upon traversal of A. If no such node exists, this will return null.
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
B(): returns the Node object that exists upon traversal of B. If no such node exists, this will return null.
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
C(): returns the Node object that exists upon traversal of C. If no such node exists, this will return null.
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
getUrl(): returns the URL string that the Node represents.
-----------------------------------------------------------------------------------------------------------

The set of recommended URLs should not include any URLs that the current user has personally saved him/herself.
The graph contains no cycles and its edges are uni-directional.
Implement getRecommendedUrls(userToken, URL)

------
Input:
-------
userToken(String), URL(String)

--------
Return:
---------
A collection of recommended URLs (Collection of Strings)