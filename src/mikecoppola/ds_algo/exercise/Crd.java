package mikecoppola.ds_algo.exercise;

import java.util.LinkedHashSet;


/*  
 * I stored all user data (userToken & urls) in a BST
 *  because we don't know how many users we are going to have and 
 *  BST is a reliable O(Log n) because no matter how big the tree gets 
 *  add, deletions and searches will remain in constant time.
 *  
 *  NOTE: This was assuming we are running on mobile:, therefore, 
 *  I like  a tree data structure because it gives us a very good balance
 *  of memory utilization and speed. With a HashMap with tens of  thousands of entries
 *  we could possibly risk an Out Of Memory error on a mobile device . 
 *  Android also has the SparseArray which is a more memory efficient than
 *  HashMap but you must key on an Integer and they are not indicated for large collections
 *  
 *  
 *  The URls themselves in a TreeNode Object are defined by:
 *  
 *   HashMap<String,LinkedHashSet<String> >
 *  
 *   
 *  HashMap lookup is speedy O(1) best case
 *  scenario. The key represents the domain (Problem #2 ). The value is a LinkedHashSet
 *  containing all the urls for that domain. I used a  LinkedHashSet because it ensures
 *  uniqueness and is O(1) for operations like lookup which we need.
 *  
 *   
 *   
 *  ** Test Cases are located in the TestCRD.java file. **
 *  
 *  
 * */
public class Crd {
	
	private Tree mTree; // Binary Search Tree
	
	
	public Crd(){
		
		mTree = new Tree();
	}
	
	public boolean saveUrl(String userToken, String url){
		
		return mTree.insert(userToken, url);
	}
	
	
	public LinkedHashSet<String> getUrls(String userToken){
		
		
		return mTree.getUrls(userToken);
	}
	
	
	public boolean removeUrl(String userToken, String url){
		
		return mTree.removeUrl(userToken , url);
	}
	
	
	
	public LinkedHashSet<String> getUsersByDomain(String domain){
		
		return mTree.getUsersByDomain(domain);
	}
	

}
