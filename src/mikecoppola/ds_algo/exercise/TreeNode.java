package mikecoppola.ds_algo.exercise;

import java.util.HashMap;
import java.util.LinkedHashSet;

public class TreeNode {
	
	public String userToken;
	public HashMap<String,LinkedHashSet<String> > mapUserUrls;
	public TreeNode left, right;
	
	
	public TreeNode(String user, String domain, String url){
		
		userToken = user;
		
		mapUserUrls = new HashMap <String,LinkedHashSet<String> >();
		
		LinkedHashSet <String>lhs = new LinkedHashSet<String>();
		lhs.add(url);
		
		mapUserUrls.put(domain, lhs);
	}
	
}
