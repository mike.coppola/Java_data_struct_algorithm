package mikecoppola.ds_algo.exercise;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;


public class Tree {
	
	private TreeNode root; 
    private TreeNode mNodeUserToken;
    private LinkedHashSet<String> mListUsersByDomain;
	
	public boolean insert( String userToken , String url){
		
		
		 if (root == null) { // must handle case of empty tree first
	         root = new TreeNode(userToken,extractDomain(url),url);
	         return true;
	      }
	      TreeNode treeNode = root; // start search downward at root
	      while (true) {
	         if (userToken.compareTo(treeNode.userToken) < 0) { // look left
	            if (treeNode.left != null) 
	            	treeNode = treeNode.left;
	            else { treeNode.left = new TreeNode(userToken,extractDomain(url), url); 
	            		break; }
	         }
	         else if (userToken.compareTo(treeNode.userToken) > 0) { // look right
	            if (treeNode.right != null) 
	            	treeNode = treeNode.right;
	            else { treeNode.right = new TreeNode(userToken , extractDomain(url), url);
	            		break; }
	         }
	         else{
	        	 // found It! So Don't insert , but add url if not there:
	        	 if(  nodeContainsUrl(treeNode,url) )
	     				return false;
	     		 else{
	     			
	     			 String domain = extractDomain(url);
	     			 LinkedHashSet<String> lhs = treeNode.mapUserUrls.get(domain);
	     			 if( lhs != null){
	     				lhs.add(url);
	     			 }else{
	     				lhs = new LinkedHashSet<String>();
	     				lhs.add(url);
	     				treeNode.mapUserUrls.put(domain,lhs);
	     			 }
	     		}
	     		break; 
	         }
	      }
		
	      return true;
	}
	
	private boolean nodeContainsUrl(TreeNode node, String url){
		
		LinkedHashSet<String> lhs = node.mapUserUrls.get(extractDomain(url));
		if(lhs != null){
			if(lhs.contains(url))
				return true;
		}
		return false;
	}
	
	 
	 public LinkedHashSet<String> getUrls(String userToken) {
		  
		   TreeNode node = traverseBasedOnUser(root, userToken);
		   
		   if(node != null && node.mapUserUrls != null){
			   
			   //we could also return HashMap of <domain,urls>
			   //in case caller is interested in that
			   //but we will return ALL urls for this user instead
			   
			   LinkedHashSet<String> tmpLhs = new LinkedHashSet<String>();
			   for (HashMap.Entry<String, LinkedHashSet<String> > entry : node.mapUserUrls.entrySet())
		       {
				   LinkedHashSet<String> value = (LinkedHashSet<String>) entry.getValue();
		           if(value != null){
		        	   tmpLhs.addAll(value);  
		           }
		       }
			   return tmpLhs;
		   }
		   
		   return null;
	 }
	 
	 public boolean removeUrl(String userToken, String url) {
		  
		   TreeNode node = traverseBasedOnUser(root, userToken);
		   
		   String domain = extractDomain(url);
		   if(node != null && node.mapUserUrls.containsKey(domain) ){
			   
			   LinkedHashSet<String> lhs  = node.mapUserUrls.get(domain);
			   if( lhs.contains(url)){
				   lhs.remove(url);
				   return true;
			   }
			}
		   
		   return false;
	}
	 
	public LinkedHashSet<String> getUsersByDomain(String domain){
		  
		  mListUsersByDomain = new LinkedHashSet<String>();
		  
		  traverseBasedOnDomain(root,domain );
		  
		  return mListUsersByDomain;
	}
	
	private TreeNode traverseBasedOnDomain(TreeNode node, String domain)
	 {
		   TreeNode result = null;
	       if (node == null)
	           return null;
	       
	       if(node.mapUserUrls != null &&  node.mapUserUrls.get(domain) != null  ){
	    	   mListUsersByDomain.add(node.userToken);
	       }
	       
	       if (node.left != null)
	           result = traverseBasedOnDomain(node.left,domain);
	       
	       if (result == null)
	           result = traverseBasedOnDomain(node.right,domain);
	       
	       return result;
	  }
	 
	 private TreeNode traverseBasedOnUser(TreeNode node, String userToken)
	 {
		   TreeNode result = null;
	       if (node == null)
	           return null;
	       
	       if(node.userToken.equals(userToken)){
	    	   return node;
	       }
	       
	       if (node.left != null)
	           result = traverseBasedOnUser(node.left,userToken);
	       
	       if (result == null)
	           result = traverseBasedOnUser(node.right,userToken);
	       
	       return result;
	  }
	 
	 
	  /*
	  * Returns domain from full url;
	  * 
	  * */
	  public String extractDomain(String fullUrl){
			
		    //ie: "http://www.amazon.com"
		    int end_www =  fullUrl.indexOf("www.");
		    end_www += 4;
		    int begin_com =  fullUrl.indexOf(".com");
		    
			String domain = fullUrl.substring(end_www,begin_com);
			
			return domain;
	  }
	 
}
