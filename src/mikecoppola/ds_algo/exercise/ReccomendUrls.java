package mikecoppola.ds_algo.exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/*
 *  KARTHIK NOTE: 
 * 
 *  This is Pseudocode to deal with the problem of finding
 *  the node K hops away from a starting node.
 *  
 *  One possible way is to use a BFS (Breadth First Search)
 *  or possibly use Dijkstra's algorithm, however BFS implementations I have dealt with are
 *  designed more for entire graph traversal and  Dijkstra finds the
 *  shortest paths in length from a to b and just seemed like more than I needed. 
 *  Plus based on he requirements of the A(), B() & C() methods of the 
 *  Node class which returns the Node object that exists upon traversal of
 *  A(), B() & C() respectively I went down a different road.
 *  
 *  I decided to use a recursive method that explores A(), B() & C()
 *  values for each node beginning at the start Node and then each subsequent node
 *  but will keep track of hops and will make sure the hops <= k AND if k hops are
 *  found from the start node it will also verify that the userToken does not match the 
 *  userToken provided.   
 * *
 * */
public class ReccomendUrls {
	
	public static int K = 3; // Max number of hops.
	
	private ArrayList<String> mListUrls;
	private String mUserToken;
	
	class Node  
	{  
		String userToken;
		String  url;  
		boolean visited; 
		
		Node aEdge;
		Node bEdge;
		Node cEdge;

		Node(String url,String userToken, Node a,Node b,Node c)  
		{  
			this.url = url;  
			this.userToken = userToken;
			this.aEdge = a;
			this.bEdge = b;
			this.cEdge = c;
		}
		

		public Node A(){
			return aEdge; 
		}
		
		public Node B(){
			return bEdge; 		
		}

		public Node C(){
			return cEdge; 
		}
	}
	
	
	
	public ReccomendUrls(ArrayList<Node> listNodes) {
		
		mListUrls = new ArrayList<String>();
	}

	
	public ArrayList<String>  getRecommendedUrls(String userToken, String url){
		
	  mUserToken = userToken;
	  Node startNode = getNode(url);
	  
	  printKDistanceUrl(startNode, 0 );
	  
	  return mListUrls;
	  
	}
	
	private void printKDistanceUrl(Node node, int numHops){
		
		if(numHops == K ){
			
			//make sure this is not a user created by our user
			if( !node.url.equals(mUserToken))
				mListUrls.add(node.url);
			
			//no matter what just return as we reached max K of 3!
			return;
		}
		
		if(node.A() != null){
			printKDistanceUrl(node.A(),numHops +1);
		}
		
		if(node.B() != null){
			printKDistanceUrl(node.B(),numHops +1);
		}
		
		if(node.C() != null){
			printKDistanceUrl(node.C(),numHops +1);
		}
	}
	
	
	
	public Node getNode(String url){
		
		//just mocked for now but...get node based on url magic here!
		
		return new Node("http//:www.notexists.com","1234" ,null,null,null);
	}
	

}
