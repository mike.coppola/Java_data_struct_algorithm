package test;

import static org.junit.Assert.*;

import java.util.LinkedHashSet;
import org.junit.Test;
import mikecoppola.ds_algo.exercise.Crd;


public class TestCRD {

	
	@Test
	public void testCrd() {
		
		Crd crd = new Crd();
		
		//---test creation ---
		assertTrue( crd.saveUrl("1", "http://www.google.com"));
		assertTrue( crd.saveUrl("1", "http://www.amazon.com"));
		assertTrue( crd.saveUrl("2", "http://www.stackoverflow.com"));
		assertTrue( crd.saveUrl("2", "https://www.developer.android.com/index.html"));
		assertTrue( crd.saveUrl("3", "http://www.yahoo.com"));
		assertTrue( crd.saveUrl("3", "http://www.bastilleagency.com") );
		assertTrue( crd.saveUrl("4", "http://www.yahoo.com/login") );
		
		//should fail as url already exists for user 3
		assertFalse( crd.saveUrl("3", "http://www.bastilleagency.com") );
		
		
		
		
		//---test reads ---
		LinkedHashSet <String>urls;
		urls = crd.getUrls("2");
		assertNotNull( urls);
        assertTrue( urls.contains("http://www.stackoverflow.com") );
        
        //should fail as user 55 does not exist:
		assertNull( crd.getUrls("55"));
		
		urls = crd.getUrls("2");
		assertEquals(urls.size(), 2 );
		
		
		
		//---test deletion ---
		assertTrue ( crd.removeUrl("1", "http://www.google.com"));
		
		//make sure "http://www.google.com" was in fact deleted:
		assertFalse ( crd.removeUrl("1", "http://www.google.com"));
		
		assertFalse ( crd.removeUrl("3", "http://www.iDontExist.com"));
		assertFalse ( crd.removeUrl("55", "http://www.iDontExist.com"));
		
		
		//--- get users by domain---
		LinkedHashSet<String> users = crd.getUsersByDomain("yahoo");
		assertEquals(users.size(), 2 );
	}
	
}
